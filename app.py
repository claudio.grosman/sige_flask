from flask import (
    Flask,
    render_template,
    request
)

app = Flask(__name__)

@app.route("/")
@app.route("/login", methods=['GET', ' POST'])
def home():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        return flask.redirect('/about')
    return render_template('login.html')

@app.route("/about")
def about():
    return render_template('about.html')

if __name__ == '__main__':
    app.run(debug=True)
